var dbConn = require('../../config/db.confog');

var employee =function(employee) {
    this.first_name = employee.first_name;
    this.last_name = employee.last_name;
    this.email = employee.email;
    this.phone = employee.phone;
    this.organization = employee.organization;
    this.designatio = employee.organization;
    this.salary = employee.salary;
    this.status = employee.status ? employee.status : 1;
    this.created_at = new Date();
    this.updated_at = new Date();

}

// get all employees
employee.getAllEmployees = (result) => {
    dbConn.query('SELECT * FROM employees',(err,res) => {
        if(err){
            console.log('Error while fetching employees',err);
            result(null,err);
        }else{
            console.log('employees fetched successfully');
            result(null,res);
        }

    })
}

//get employee by ID from db
employee.getEmployeeByID  = (id,result)  => {
    dbConn.query('SELECT * FROM employees WHERE id=?', id, (req,res) => {
        if(err){
            console.log('Error while fetching employees by id', err);
            result(null,err);
        }else{
            result(null,res);
        }
    })
}

//create new employee
employee.createEmployee  = (employeeReqData,result)  => {
    bdConn.query('INSERT INTO employee SET ?',employeeReqData, (err,res)  => {
        if(err){
            console.log('Error while inserting data');
            result(null,err);
        }else{
            console.log('employee created successfully');
            result(null,res);
        }
    })
}

// update employee
employee.updateEmployee = (id,employeeReqData,result) =>{
    dbConn.query("UPDATE employees SET first_name=?,last_name=?,email=?,phone=?,organization=?,designation=?,salary=?, status=? WHERE id = ?",
     [employeeReqData.first_name,employeeReqData.last_name,employeeReqData.email,employeeReqData.phone,employeeReqData.organization,employeeReqData.designation,employeeReqData.salary,employeeReqData.status, id], (err,res)  =>{
            if(err){
                console.log('Error while updating the employee');
                result(null,err);

            }else{
                console.log('Employee updated successfully');
                result(null,res);
            }
        });
    }
    //delete employee
  /*  Employee.deleteEmployee = (id,result) =>{
        dbConn.query('DELETE FROM employees WHERE id=?', [id], (err,res) =>{
            if(err){
            console.log('Error while deleting employee');
            result(null,err);
        }else{
            result(null,res);
        }
    })*/

    
    employee.deleteEmployee = (id,result) =>{
        dbConn.query('"UPDATE employees SET is_deleted=? WHERE id=?', [1,id], (err,res) =>{
            if(err){
            console.log('Error while deleting employee');
            result(null,err);
        }else{
            console.log('employee deleted successfully');
            result(null,res);
        }
    })
}

module.exports  = employee;